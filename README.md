# Forge Wheel of Fortune 

[![Atlassian license](https://img.shields.io/badge/license-Apache%202.0-blue.svg?style=flat-square)](LICENSE) [![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg?style=flat-square)](CONTRIBUTING.md)

This is a simple example which generates a spinning wheel from the contents of a table on the page and selects a random row.  

![Example gif](./docs/images/wheel.gif)

## Installation

If this is your first time using Forge, the [getting started](https://developer.atlassian.com/platform/forge/set-up-forge/) guide will help you install the prerequisites.

If you already have a Forge environment setup you can deploy this example straight away. Visit our [example apps](https://developer.atlassian.com/platform/forge/example-apps/) page for installation steps.


## Documentation

The app's [manifest.yml](./manifest.yml) contains two modules:

1. A [macro module](https://developer.atlassian.com/platform/forge/manifest-reference/#macro) that specifies the metadata displayed to the user in the Confluence editor's quick insert menu.
2. A corresponding [function module](https://developer.atlassian.com/platform/forge/manifest-reference/#function) that implements the macro logic.

The function logic is implemented wholly in a single file [src/index.tsx](./src/index.tsx).
 
The app's UI is implemented using these features:

- One [Editor Macro](https://developer.atlassian.com/platform/forge/manifest-reference/#macro) that displays a spinning wheel visualization.

- A [Button](https://developer.atlassian.com/platform/forge/ui-components/button/) to trigger the wheel to spin.

- [Image](https://developer.atlassian.com/platform/forge/ui-components/image) component.

- [SVG](https://en.wikipedia.org/wiki/Scalable_Vector_Graphics) image format to dynamically generate an image. 

- [useProductContext](https://developer.atlassian.com/platform/forge/ui-hooks-reference/) to get the id of the current page.  

- [useAction](https://developer.atlassian.com/platform/forge/ui-hooks-reference/#useaction) hooks.

- [api.asUser().requestConfluence](https://developer.atlassian.com/platform/forge/runtime-api-reference/#requestconfluence) to read the contents of the current page.

## Contributions

Contributions to Forge Wheel of Fortune are welcome! Please see [CONTRIBUTING.md](CONTRIBUTING.md) for details. 

## License

Copyright (c) 2020 Atlassian and others.
Apache 2.0 licensed, see [LICENSE](LICENSE) file.